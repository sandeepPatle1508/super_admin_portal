require 'test_helper'

class ConsignmentsControllerTest < ActionController::TestCase
  setup do
    @consignment = consignments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:consignments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create consignment" do
    assert_difference('Consignment.count') do
      post :create, consignment: {  }
    end

    assert_redirected_to consignment_path(assigns(:consignment))
  end

  test "should show consignment" do
    get :show, id: @consignment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @consignment
    assert_response :success
  end

  test "should update consignment" do
    put :update, id: @consignment, consignment: {  }
    assert_redirected_to consignment_path(assigns(:consignment))
  end

  test "should destroy consignment" do
    assert_difference('Consignment.count', -1) do
      delete :destroy, id: @consignment
    end

    assert_redirected_to consignments_path
  end
end
