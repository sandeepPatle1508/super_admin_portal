class Vendor
	include Her::Model

  	validates :name, :address, :city, :country, :zip_code, :phone_number, :presence => true
end
