class ConsignmentsController < ApplicationController
  before_filter :get_vendor_organization, :only => [:new, :edit]
  # GET /consignments
  # GET /consignments.json
  def index
    @consignments = Consignment.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @consignments }
    end
  end

  # GET /consignments/1
  # GET /consignments/1.json
  def show
    @consignment = Consignment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @consignment }
    end
  end

  # GET /consignments/new
  # GET /consignments/new.json
  def new
    @consignment = Consignment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @consignment }
    end
  end

  # GET /consignments/1/edit
  def edit
    @consignment = Consignment.find(params[:id])
  end

  # POST /consignments
  # POST /consignments.json
  def create
    @consignment = Consignment.new(params[:consignment])
    respond_to do |format|
      if @consignment.save
        if generate_qr_code
          format.html { redirect_to @consignment, notice: 'Consignment and QR code was successfully created.' }
          format.json { render json: @consignment, status: :created, location: @consignment }
        else
          format.html { redirect_to @consignment, notice: 'Consignment was successfully created. but failed to generate QR code.' }
          format.json { render json: @consignment, status: :created, location: @consignment }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @consignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /consignments/1
  # PUT /consignments/1.json
  def update
    @consignment = Consignment.find(params[:id])
    assign_values_to_object(@consignment, params[:consignment])

    respond_to do |format|
      if @consignment.save
        format.html { redirect_to @consignment, notice: 'Consignment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @consignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consignments/1
  # DELETE /consignments/1.json
  def destroy
    @consignment = Consignment.find(params[:id])
    @consignment.destroy

    respond_to do |format|
      format.html { redirect_to consignments_url }
      format.json { head :no_content }
    end
  end

  def history
    @consignment = Consignment.find(params[:consignment_id])
    @histories = ConsignmentHistory.where(consignment_id: params[:consignment_id]).all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @histories }
    end
  end

  def new_comment
    @consignment = Consignment.find(params[:consignment_id])
  end

  def save_comment
    @consignment_history = ConsignmentHistory.new(params[:comment])

    if @consignment_history.save        
      redirect_to "/consignment/#{params[:consignment_id]}/history", :flash => { :success => "Comment added" }
    else        
      redirect_to "/consignment/#{params[:consignment_id]}/history", :flash => { :error => "Error in save" }
    end
  end

  private
  def get_vendor_organization
    @vendors = Vendor.all
    @organizations = Organization.all
    @categories = {}
    @types = {}
  end

  # Generate QR code and convert it into image
  # And save into folder
  def generate_qr_code
    begin
      qr_code_image_url = QR_CODE_IMAGE_DIR + QR_CODE_IMAGE_PREFIX + "_#{@consignment.id.to_s}_" + Time.now.to_i.to_s + ".png"
      qr_code_image_save_url = QR_CODE_IMAGE_SAVE_DIR + QR_CODE_IMAGE_PREFIX + "_#{@consignment.id.to_s}_" + Time.now.to_i.to_s + ".png"
      qr = RQRCode::QRCode.new( @consignment.id.to_s, :size => 4, :level => :h )
      png = qr.to_img # returns an instance of ChunkyPNG
      png.resize(250, 250).save(qr_code_image_url)

      @consignment.qr_code = qr_code_image_save_url
      @consignment.save
    rescue
      false
    end
  end
end
