class ApplicationController < ActionController::Base
  protect_from_forgery

  def assign_values_to_object(model_object, attributes)
  	attributes.each { |attribute, value| puts model_object.send("#{attribute}=", value) }
  	model_object
  end
end
