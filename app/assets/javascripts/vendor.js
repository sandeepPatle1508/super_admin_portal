$(document).ready( function () {
	

	$('.js-new_vendor_submit').on('click', function(event) {
		event.preventDefault();
		if (validateNewVendor()) {

			$('.js-new_vendor_form').submit();
		}

	});

	$('.js-new_organization_submit').on('click', function(event) {
		event.preventDefault();
		if (validateNewVendor()) {

			$('.js-new_organization_form').submit();
		}

	});
});


function validateNewVendor() {
	var returnValue = true;
	var name = $('.js-name');
	var address = $('.js-address');
	var city = $('.js-city');
	var country = $('.js-country');
	var zipCode = $('.js-zip_code');
	var phoneNumber = $('.js-phone_number');

	returnValue = isEmpty(name);
	returnValue = isEmpty(address);
	returnValue = isEmpty(city);
	returnValue = isEmpty(country);
	returnValue = isEmpty(zipCode);
	returnValue = isEmpty(phoneNumber);

	return returnValue;

}