$(document).ready( function () {

	$('.js-new_consignment_submit').on ('click', function(event) {
		event.preventDefault();
		if (validateNewConsignment()) {
			$('.js-new_consignment_form').submit();
		}
	});
	
});

function validateNewConsignment() {
	var returnValue = true;
	var title = $('.js-title');
	var deliverDate = $('.js-deliver_date');

	returnValue = isEmpty(title);
	returnValue = isEmpty(deliverDate);

	return returnValue;
}