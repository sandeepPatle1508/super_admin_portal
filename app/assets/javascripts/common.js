$(document).ready( function () {

});

function isEmpty(element) {
	var returnValue = true;

	if (element.length >0) {

		if ($.trim(element.val()).length <= 0 ) {
			returnValue = false;
			element.parent().addClass('error');
			element.parent().find('.js-error').removeClass('hide');
	}
	else {
		element.parent().removeClass('error');
		element.parent().find('.js-error').addClass('hide');

		}
	}

}